// Modules
const express = require("express");
const path = require("path");
const mongoose = require("mongoose");

// Model
const Contacts = require("./model/contact");
const Sneakers = require("./model/sneakers");
const User = require("./model/user");

const db =
  "mongodb+srv://volodymyr:vovka4544@cluster0.ihdnt.mongodb.net/node-shop?retryWrites=true&w=majority";

const app = express();

mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => {
    console.log("db is connected");
  })
  .catch((err) => {
    console.log(err);
  });
app.use(express.static("styles"));
app.use(express.static("js"));
app.use(express.static("assets"));

app.set("view engine", "ejs");
const createPath = (page) =>
  path.resolve(__dirname, "ejs-views", `${page}.ejs`);

app.listen(3000, () => {
  console.log("app running on Port 3000");
});

app.get("/", (req, res) => {
  const title = "Home";
  Sneakers.find()
    .then((sneakers) => {
      res.render(createPath("index"), { title, sneakers });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.get("/collection", (req, res) => {
  const title = "Collection";
  Sneakers.find()
    .populate("ownedBy", "ownedBy")
    .then((sneakers) => {
      console.log(sneakers);
      res.render(createPath("collection"), { title, sneakers });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.get("/shop", (req, res) => {
  const title = "Shop";
  Sneakers.find()
    .then((sneakers) => {
      res.render(createPath("shop"), { title, sneakers });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.get("/sneakers/:id", (req, res) => {
  const title = "Sneakers ";
  Sneakers.findById(req.params.id)
    .then((sneaker) => {
      res.render(createPath("sneakers"), { sneaker, title });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.get("/contacts", (req, res) => {
  const title = "Contacts";
  Contacts.find()
    .then((contacts) => {
      res.render(createPath("contacts"), { title, contacts });
    })
    .catch((err) => {
      console.log(err);
    });
});
app.get("/user", (req, res) => {
  const title = " User";
  User.find().then((user) => {
    console.log(user);
  });
});
app.post("/add-sneaker", (req, res) => {
  const snekaer = new Sneakers({
    name: "Joggers",
    description: "Joggers shoes",
    image:
      "https://image.made-in-china.com/202f0j00CDLGeIhBJfcR/Greatshoe-Hot-Selling-Man-Sneaker-Casual-Shoes-Jogging-Running-Walking-Sport-Shoes-OEM-ODM-Brand-Service-Footwear-Men-Shoes.jpg",
    ownedBy: new User({ name: "Volodymyr", surname: "Bogryashov" }),
  });
  snekaer
    .save()
    .then((result) => res.redirect("/shop"))
    .catch((error) => {
      console.log(error);
    });
});
